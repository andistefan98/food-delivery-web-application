<%@page import="java.util.ArrayList" %>
<%@page import="entities.Order" %>
<%@page import= "java.sql.*" %>
<%@page import= "javax.sql.*" %>
   
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Order Receipt</title>

<style>
.div2{
 text-align: center;
}</style>

<link rel="stylesheet" type="text/css"  href="css2/styleFinale.css">
<link rel="stylesheet" type="text/css" href="css2/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="css2/nivo-lightbox/default.css">
</head>
<body>

<%


Order order = (Order)session.getAttribute("finalorder"); 
session.setAttribute("finn", order);

 ArrayList<String> products = new ArrayList<>();
  	 ArrayList<Integer> quantities = new ArrayList<>();
  	 ArrayList<Float> unitPrices = new ArrayList<>();
  
  	 products = order.getProducts();
  	 quantities= order.getQuantities();
  	 unitPrices = order.getUnitPrices();
     float finalPrice = order.getFinalPrice();
  	//int maxx = products.size();
   String restName = (String)session.getAttribute("numeRestaurant");
   int idd = (int)session.getAttribute("restid");
    String adresa = (String)session.getAttribute("adresa");
    String nume = (String)session.getAttribute("numeA");
    int idCl = Integer.parseInt((String)session.getAttribute("idCl"));
    
    session.setAttribute("adresaa", adresa);
    session.setAttribute("numee", nume);
   //database for courier 
    PreparedStatement ps1 = null;
	 ResultSet rs1 = null;
		   
		   String urll="http://localhost:8010/FoodDelivery/ordersDispatcher.jsp?myparam1={adresa}&myparam2={nume}&myparam3={finalPrice}";
	       session.setAttribute("url", urll);
		    
	 Class.forName("com.mysql.jdbc.Driver");
     Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false","root", "partener32");
   
     String query = "SELECT * FROM dispatcher WHERE iddispatcher=?";
     ResultSet rs = null;
     PreparedStatement ps;
     ps = con.prepareStatement(query);
	 ps.setInt(1, idd);

	 rs = ps.executeQuery();
	 rs.next();
	 String dispName = rs.getString("Name");
	 int phone = rs.getInt("Phone");
	 int estimatedDelivery = 30;
	 
	 String query2 = "INSERT INTO orders (Client,Phone,Restaurant,Delivery,idclient,iddispatcher,Products,Quantites,Price,idrestaurant)  VALUES (?,?,?,?,?,?,?,?,?,?)";
     ResultSet rs2 = null;
     PreparedStatement ps2 = con.prepareStatement(query2);
     
     System.out.println(nume + "  " + phone + "  " + restName + "  " + adresa + "  " + finalPrice);
	 ps2.setString(1,adresa );
	 //ps2.setString(2, (String)session.getAttribute("userclient"));

	 ps2.setInt(2, phone);
	 ps2.setString(3,restName);
	 ps2.setString(4,nume );
	 ps2.setInt(5,idCl);
	 ps2.setInt(6,idd);
	 ps2.setString(7,order.getProducts().toString());
	 ps2.setString(8,order.getQuantities().toString());
	 ps2.setFloat(9,finalPrice);
	 ps2.setInt(10, idd);
	 int ret = ps2.executeUpdate();
  	 %>
  
  
 <div id="main">	 
  	   	 <div class="div2">
  
  	 <div class="gap-20"></div>
               <div class="gap-20"></div>
                <div class="gap-20"></div>
               <div class="gap-20"></div>
               
  	 <div class="gap-20"></div>
               <div class="gap-20"></div>
                <div class="gap-20"></div>
               <div class="gap-20"></div>
                <div class="gap-20"></div>
                 <div class="gap-20"></div>
                  <div class="gap-20"></div>
  	 

  	 <h2>Here you can find your order details : </h2>
  	 <div class="gap-20"></div>
     <h3>Restaurant  : <%= restName %></h3>
  	
  	<% 
  	for(int i=0; i< products.size();i++)
  	 {
  		estimatedDelivery += 10 ;
  		 %> 
  	 
    	 <h3><%= products.get(i) %> ...................................<%= unitPrices.get(i) * quantities.get(i) %>$ </h3>
 
   <%
  	 }
  	session.setAttribute("cartempty", "yes");
  	
  	
  	 %>
   	  
  <h3> Totals : <%= finalPrice %>$</h3>
    <div class="gap-20"></div>
    <div class="gap-20"></div>
  <h2>Shipping info : </h2>
  <h3>Courier name: <%= dispName %></h3>
  <h3>Courier phone number: <%= phone %></h3>
   <h3>To be delivered to: <%= adresa %></h3>
      <h3>At the address: <%= nume %></h3>
  <div class="gap-20"></div>
    <div class="gap-20"></div>
  <h2>Payment details</h2>
 
  <%if(session.getAttribute("payment").equals("cash")){%>
		 <h3>Payment : Cash on receival </h3>
	<% }
  else{
  %>
   <h3>Payment : Card </h3>
    <h3>Name : <%= session.getAttribute("nameOnCard") %> </h3>
   <h3>Card Number : **** **** **** <%=session.getAttribute("cardNumber").toString().substring(12) %> </h3>
  <%} %>
  <div class="gap-20"></div>
    <div class="gap-20"></div>
  <h3>Estimated delivery time: <%= estimatedDelivery %></h3>
  <div class="gap-20"></div>
    <div class="gap-20"></div>
  <input type="button" class="butonulMeu2" onclick="window.location.href='http://localhost:8010/FoodDelivery/pageRestaurantCView.jsp'" name="place_order" value="Back to site" />
  
  </div>
  </div>
</body>
</html>