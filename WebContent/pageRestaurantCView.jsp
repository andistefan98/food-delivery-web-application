<%@page import="java.util.ArrayList" %>
<%@page import="java.sql.Connection" %>
<%@page import="common_things.DB_Connection" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="java.sql.PreparedStatement"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Food Delivery</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css"  href="css2/style.css">
<link rel="stylesheet" type="text/css" href="css2/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="css2/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
    
         <li><a href="indexClient.jsp">Home</a></li>
         <li><a href="pageRestaurantCView.jsp">Restaurants</a></li>
         <li><a href="aboutPage.html">About</a></li>
         <li><a href="contactPage.html">Contact</a></li>
         <li><a href="<%=request.getContextPath()%>/LogoutServlet">Logout</a></li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
</nav>
<%
 
Connection con = null;
Statement statement = null;
ResultSet resultSet = null;

ArrayList<Integer> restId = new ArrayList<Integer>(); 
ArrayList<String> restName = new ArrayList<String>(); 
ArrayList<String> restLocation= new ArrayList<String>();
ArrayList<String> restDescr= new ArrayList<String>();
ArrayList<String> restPhotos = new ArrayList<String>();
ArrayList<String> photoPaths = new ArrayList<String>();

try
{
con = DB_Connection.getConnection();
statement = con.createStatement();

resultSet = statement.executeQuery("select idrestaurant,Name,Location,Description,Picture from restaurant");

while(resultSet.next())
{
restId.add(resultSet.getInt("idrestaurant"));
restName.add(resultSet.getString("Name"));
restLocation.add(resultSet.getString("Location"));
restDescr.add(resultSet.getString("Description"));
restPhotos.add(resultSet.getString("Picture"));


String path = resultSet.getString("Picture");
photoPaths.add(path);
System.out.println(path);
%>



 <% 
}
}
catch(Exception e){}

int i = 0;



 %>
 
 

<!-- Portfolio Section -->
<div id="portfolio">
  <div class="section-title text-center center">
    <div class="overlay">
      <h2>Restaurants in your town</h2>
      <hr>
     
    </div>
  </div>
  <div class="container">
 
    <div class="row">
      <div class="portfolio-items">
    
    
  <%
  for(String ph : photoPaths){
	  i++;
  %>
  
   <div class="col-sm-6 col-md-4 col-lg-4 breakfast">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="<%= ph %>" title="Dish Name" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4><%= restName.get(photoPaths.indexOf(ph)) %></h4>
                
              </div>
              <img src="<%= ph %>" class="img-responsive" alt="Project Title"> </a> </div>
              <button class="btn btn-success btn-lg float-right" onclick="window.location.href='http://localhost:8010/FoodDelivery/mynextPageC.jsp?myparam=<%=i%>'"><%=restName.get(photoPaths.indexOf(ph))%></button>
          </div>
        </div>
  
  
 
  <% }
  %>
         
       
        
        
        
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/SmoothScroll.js"></script> 
<script type="text/javascript" src="js/nivo-lightbox.js"></script> 
<script type="text/javascript" src="js/jquery.isotope.js"></script> 
<script type="text/javascript" src="js/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="js/contact_me.js"></script> 
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
