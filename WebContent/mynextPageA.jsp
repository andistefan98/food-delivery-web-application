<%@page import="java.util.ArrayList" %>
<%@page import="java.sql.Connection" %>
<%@page import="common_things.DB_Connection" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="java.sql.PreparedStatement"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Food Delivery</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css"  href="css2/style.css">
<link rel="stylesheet" type="text/css" href="css2/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="css2/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
      <li><a href="indexAdmin.jsp">Home</a></li>
         <li><a href="pageRestaurantAView.jsp">Restaurants</a></li>
         <li><a href="addRestaurant.jsp">Add Restaurant</a></li>
         <li><a href="<%=request.getContextPath()%>/LogoutServlet">Logout</a></li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
</nav>
<%

 int id = Integer.parseInt(request.getParameter("myparam"));

 
 ArrayList<Integer> restId = new ArrayList<Integer>(); 
 String restName ="";
 String restLocation ="";
 String restDescr="";
 String restPhotos="";
 String photoPaths="";
 
 try
 {
 
 
 DB_Connection newCon = new DB_Connection();
 Connection con = newCon.getConnection();
 PreparedStatement ps = null;
 ResultSet resultSet = null;
 String stt = "select Name,Location,Description,Picture from restaurant where idrestaurant=?";
 ps= con.prepareStatement(stt);
 ps.setInt(1, id);
 
 resultSet = ps.executeQuery();

 resultSet.next();
 restName = resultSet.getString("Name");

 restLocation=resultSet.getString("Location");
 restDescr=resultSet.getString("Description");
 restPhotos=resultSet.getString("Picture");


 

 
 }
 catch(NullPointerException e){
	 e.getStackTrace();
	 System.out.print("nullll");
 }
 
 session.setAttribute("restaurant", restName);
 session.setAttribute("restid", id);
 %>
 
 
 
<!-- Header -->
<header id="header">
  <div class="intro">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="intro-text">
            <h1><%= restName %></h1>
            <h2><%= restLocation %></h2>
            <p>Restaurant / Coffee </p>
            <a href="#about" class="btn btn-custom btn-lg page-scroll">Discover Story</a> 
             <a href="addDish.jsp" class="btn btn-custom btn-lg page-scroll">Add Dish</a> </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- About Section -->
<div id="about">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-6 ">
        <div class="about-img"><img src="img/about.jpg" class="img-responsive" alt=""></div>
      </div>
      <div class="col-xs-12 col-md-6">
        <div class="about-text">
          <h2>Our Restaurant</h2>
          <hr>
          <p><%= restDescr %></p>
         
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Portfolio Section -->
<div id="portfolio">
  <div class="section-title text-center center">
    <div class="overlay">
      <h2>Menu</h2>
      <hr>
     
    </div>
  </div>
  <div class="container">
 
    <div class="row">
      <div class="portfolio-items">
    
    
  <%
  DB_Connection newCon = new DB_Connection();
  Connection con = newCon.getConnection();
  PreparedStatement ps2 = null;
  ResultSet resultSet2 = null;
  String stt2 = "select * from dish where RestaurantId=?";
  ps2= con.prepareStatement(stt2);
  ps2.setInt(1, id);
  
  resultSet2 = ps2.executeQuery();

  while(resultSet2.next()){
  String dishName = resultSet2.getString("Name");

  Float dishPrice=resultSet2.getFloat("Price");
  String photoo=resultSet2.getString("Photo");
  session.setAttribute("todelete", dishName); 
  %>
  
   <div class="col-sm-6 col-md-4 col-lg-4 breakfast">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="<%= photoo %>" title="Dish Name" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4><%= dishName %></h4>
                <h4><%= dishPrice %></h4>
              </div>
              
              <img src="<%= photoo %>" class="img-responsive" alt="Project Title"> </a> </div>
                <form action="DeletiontControler" method="post">
        <input type="submit" name="delete_item" value="Delete" />
        <input type="hidden" name="item" value="<%=dishName%>" />
 
       
                </form>
          
          </div>
     
        </div>
 
 
  <% }
  %>
         
     
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/SmoothScroll.js"></script> 
<script type="text/javascript" src="js/nivo-lightbox.js"></script> 
<script type="text/javascript" src="js/jquery.isotope.js"></script> 
<script type="text/javascript" src="js/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="js/contact_me.js"></script> 
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
