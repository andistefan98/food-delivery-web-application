<%@page import="java.util.ArrayList" %>
<%@page import="entities.Order" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"  href="css2/style.css">
<title>Confirmation</title>
<style>
table {
  border-collapse: collapse;
  width: 92%;
}

.div2{
 text-align: center;
}
th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>

<%
  
  

%>


    
    <%
    Order order = (Order)session.getAttribute("orderdet");
    ArrayList<String> products = new ArrayList<>();
  	 ArrayList<Integer> quantities = new ArrayList<>();
  	 ArrayList<Float> unitPrices = new ArrayList<>();
  
  	 products = order.getProducts();
  	 quantities= order.getQuantities();
  	 unitPrices = order.getUnitPrices();
  	// totalPrices = order.getTotalprices();
  	float finalPrice =0 ;
  	for(int counter=0;counter<products.size();counter++) {
  	   finalPrice += quantities.get(counter)*unitPrices.get(counter);
  			   }
  	
    order.setFinalPrice(finalPrice);
  	// float finalPrice = order.getFinalPrice();
  	session.setAttribute("finalorder", order);
  	
  	
  	request.setAttribute("finalPrice", finalPrice);
  	
  	
  	session.setAttribute("finalPrice", finalPrice);
  	 int maxx = products.size();
  	 %>
  	
  	<table>
  <tr>
    <th>Product</th>
    <th>Quantity</th>
    <th>Price</th>
  </tr>
<% 
  	for(int i=0; i< products.size();i++)
  	 {
  		 %> 
  	 
    
    <tr>
    <td><%= products.get(i)%></td>
    <td><%=quantities.get(i)%></td>
    <td><%=unitPrices.get(i)%></td>
  </tr>

   <%
  	 }
  	 %>
</table>
 <div class="gap-20"></div>
           
            
            
           <div class="div2">
            <h2><%=finalPrice %>$</h2>
            <div class="gap-20"></div>
            <h3>Forgot something?</h3>
            <input type="button" class="butonulMeu2" value="Back to cart" onclick="history.back()">
              <div class="gap-20"></div>
               <div class="gap-20"></div>

             <h3>Confirm order</h3>
            <form action="<%=request.getContextPath()%>/sendNotification?finalPrice=<%=session.getAttribute("finalPrice")%>" method="post">
            <input type="submit" class="butonulMeu2" name="place_order" value="  Payment  " />
           </form>
           </div>

</body>
</html>