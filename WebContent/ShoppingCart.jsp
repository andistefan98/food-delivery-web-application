<%@page import="java.util.ArrayList" %>
<%@page import="entities.Order" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Shopping Cart</title>
<style>
body{}
table{
width:800 px;
margin: auto;
text-align : center;
table-layout:fixed;
}

table, tr,td ,th{
color:black;
border-collapse: collapse;
border:1px solid #4CAF50 ;
font-size: 18px;
font-family:Arial;
background:  linear-gradient(top,#3c3c3c 0%, #4CAF50 100%);
background: -webkit-linear-gradient(top,#3c3c3c 0% , #4CAF50 100%);
}

</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel ="stylesheet" type="text/css" href="css/styleRestView.css"> 
<link rel="stylesheet" type="text/css"  href="css2/style.css">
<link rel="stylesheet" type="text/css" href="css2/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="css2/nivo-lightbox/default.css">
</head>
 
<body>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="main">
    <nav>
    <ul>
         <li>Shopping Cart</li>
      </ul>
    
    </nav>
 </div>
            <div class="gap-20"></div>
            <div class="gap-20"></div>
            <div class="gap-20"></div>
        <%
        String cartEmpty = (String)session.getAttribute("cartempty");
      //  if(cartEmpty.equals("yes")){
        //	"${cart.lineItemCount}" = 0 ;
      //  }
     ArrayList<String> products = new ArrayList<>();
   	 ArrayList<Integer> quantities = new ArrayList<>();
   	 ArrayList<Float> unitPrices = new ArrayList<>();
   	 ArrayList<Float> totalPrices = new ArrayList<>();
         %>

<table width="75%" border="1">
  <tr bgcolor="#4CAF50">
    <td>Product</td>
    <td>Quantity</td>
    <td><strong>Unit
      Price</font></strong></td>
    <td>Total</td>
  </tr>
  <jsp:useBean id="cart" scope="session" class="bean.CartBean" />
  <c:if test="${cart.lineItemCount==0}">
  <tr>
  <td colspan="4"><font size="3.5" face="Verdana, Arial, Helvetica, sans-serif">- Cart is currently empty -<br/>
  </tr>
  </c:if>
  <c:forEach var="cartItem" items="${cart.cartItems}" varStatus="counter">
    <%//products.add("${cart.cartItems}"); %>
  <form name="item" method="POST" action="servlets/CartController">
  <tr>
    <td><c:out value="${cartItem.partNumber}"/><br/>
      <c:out value="${cartItem.modelDescription}"/></td>
    <td><input type='hidden' name='itemIndex' value='<c:out value="${counter.count}"/>'><input type='text' name="quantity" value='<c:out value="${cartItem.quantity}"/>' size='2'><%// quantities.add(Integer.parseInt("${cartItem.quantity}")); %> <input type="submit" name="action" value="Update">
         <input type="submit" name="action" value="Delete"></td>
    <td>$<c:out value="${cartItem.unitCost}"/></td>
    <%
   // System.out.println(${cartItem.unitCost});
   // unitPrices.add(Float.parseFloat(${cartItem.unitCost})); %>
    <td>$<c:out value="${cartItem.totalCost}"/></td>
        <%//totalPrices.add(Float.parseFloat("${cartItem.totalCost}")); %>
  </tr>
  </form>
  </c:forEach>
  <tr>
    <td colspan="2"> </td>
    <td> </td>
    <td><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Subtotal: $<c:out value="${cart.orderTotal}"/></font></td>
  </tr>
</table>


            <div class="gap-20"></div>
            <div class="gap-20"></div>
            
<input type="button" class="butonulMeu3" value="Menu" onclick="history.back()">


  <form name="item" method="POST" action="servlets/CartController">
      <input type="button" class= "butonulMeu4" name="action" value="Delete All">
      </form>
 
 <input type="button" class="butonulMeu5" onclick="window.location.href='http://localhost:8010/FoodDelivery/PlaceOrder.jsp'" name="place_order" value="Place order" />
 

<script type="text/javascript" src="js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/SmoothScroll.js"></script> 
<script type="text/javascript" src="js/nivo-lightbox.js"></script> 
<script type="text/javascript" src="js/jquery.isotope.js"></script> 
<script type="text/javascript" src="js/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="js/contact_me.js"></script> 
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>