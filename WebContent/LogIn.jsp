<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="css/logsignstyle.css">
  </head>
  <body>
  
 <form name="form" action="<%=request.getContextPath()%>
/LoginServlet" method="post">
<div class="login-box">
  <h1>Login</h1>
  <div class="textbox">
    <i class="fas fa-user"></i>
    <input type="text" placeholder="Username" name= "userName" value="" >
  </div>

  <div class="textbox">
    <i class="fas fa-lock"></i>
    <input type="password" placeholder="Password" name = "password" value="">
  </div>

  <input class="btn" type="submit"  value="Sign in">
</div>

<%
       
        String message = (String)session.getAttribute("login message");
       
       if(message != null){
    	   out.println(message);
    	   session.removeAttribute("login message");
       }
       
       %>

    </form>
  </body>
</html>