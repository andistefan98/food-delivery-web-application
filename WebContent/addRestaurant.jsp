<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="css/addrestaurantstyle.css">
  </head>
  <body>
  
  
  <div id="main">
    <nav>
       <ul>
         <li><a href="indexAdmin.jsp">Home</a></li>
         <li><a href="pageRestaurantAView.jsp">Restaurants</a></li>
         <li><a href="addRestaurant.jsp">AddRestaurant</a></li>
         
         <li><a href="<%=request.getContextPath()%>/LogoutServlet">Logout</a></li>
      </ul>
    
    </nav>
 </div>
       
      
       <form action="addRestaurant_controller.jsp">
       
     <div class="login-box">
        <h1>New Restaurant</h1>  
         <div class="textbox">
    <i class="fas fa-user"></i>
    <input type="text" placeholder="Restaurant Name" name= "restaurantName" value="" >
  </div>
        
         <div class="textbox">
    <i class="fas fa-lock"></i>
    <input type="text" placeholder="Location" name= "restaurantLocation" value="" >
  </div>
         
         
         <div class="textbox">
    <i class="fas fa-user"></i>
    <input type="text" placeholder="Description" name= "restaurantDescription" value="" >
  </div>
  
   
       <div class="textbox">
    <i class="fas fa-lock"></i>
    <input type="text" placeholder="Dispatcher username" name= "userName" value="" >
  </div>
  
       <div class="textbox">
    <i class="fas fa-lock"></i>
    <input type="text" placeholder="Dispatcher password" name= "password" value="" >
  </div>
  
      <div class="textbox">
    <i class="fas fa-lock"></i>
    <input type="text" placeholder="Dispatcher name" name= "dispatcherName" value="" >
  </div>
  
  
      
        <div class="textbox">
    <i class="fas fa-lock"></i>
    <input type="text" placeholder="Dispatcher phone" name= "dispatcherPhone" value="" >
  </div>     
  
        
        <div class="textbox">
    <i class="fas fa-lock"></i>
    <input type="text" placeholder="Photo link" name= "photo" value="" >
  </div>
        
  
         <input class ="btn" type="submit" value="Add">
       
       </div>
       
       </form>
 

</body>
</html>