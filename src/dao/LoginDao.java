package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 
import bean.LogInBean;


import common_things.DB_Connection;
 
public class LoginDao {
 
public String authenticateUser(LogInBean loginBean)
{
 String userName = loginBean.getUserName();
 String password = loginBean.getPassword();
 
 Connection con = null;
 Statement statement = null;
 ResultSet resultSet = null;
 
 String userNameDB = "";
 String passwordDB = "";
 int roleDB = 0;
 
 try
 {
 con = DB_Connection.getConnection();
 statement = con.createStatement();
 resultSet = statement.executeQuery("select username,password,type from userstypes");
 
 while(resultSet.next())
 {
 userNameDB = resultSet.getString("username");
 passwordDB = resultSet.getString("password");
 roleDB = Integer.parseInt(resultSet.getString("type"));
 
 System.out.println(userNameDB + " " + roleDB);
 
 if(userName.equals(userNameDB) && password.equals(passwordDB) && roleDB==0 ){
	 
	 System.out.println("IS ADMIN! !!!  !");
 return "Admin_Role";
 
 }
 
 else if(userName.equals(userNameDB) && password.equals(passwordDB) && roleDB==1)
 return "User_Role";
 else if(userName.equals(userNameDB) && password.equals(passwordDB) && roleDB==2)
 return "Dispatcher_Role";
 }
 }
 catch(SQLException e)
 {
 e.printStackTrace();
 }
 return "Invalid user credentials";
}
}