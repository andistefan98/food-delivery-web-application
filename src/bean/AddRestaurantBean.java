package bean;

public class AddRestaurantBean {

	 private String userName;
	 private String password;
	 private String nameRestaurant;
	 private String location;
	 private String nameDispatcher;
	 private int phone;
	 private String description;
	 
	 
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getNameRestaurant() {
		return nameRestaurant;
	}
	public void setNameRestaurant(String nameRestaurant) {
		this.nameRestaurant = nameRestaurant;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNameDispatcher() {
		return nameDispatcher;
	}
	public void setNameDispatcher(String nameDispatcher) {
		this.nameDispatcher = nameDispatcher;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
