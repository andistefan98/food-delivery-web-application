package servlets;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Connection;



import common_things.DB_Connection;

/**
 * Servlet implementation class DeletiontControler
 */
@WebServlet("/DeletiontControler")
public class DeletiontControler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeletiontControler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		String name = (String)session.getAttribute("todelete");
		System.out.println("name + " + name);
		 DB_Connection newCon = new DB_Connection();
	
		
		try {
			 Connection con = newCon.getConnection();
		  PreparedStatement ps2 = null;
		  ResultSet resultSet2 = null;
		  String stt2 = "delete from dish where Name=?";
		  ps2= con.prepareStatement(stt2);
		  ps2.setString(1, name);
		  
		  int ret = ps2.executeUpdate();
			
		  response.sendRedirect(request.getHeader("Referer"));
		  
		}
		catch(SQLException e){
			e.getMessage();
		}
	
	}

}
