package servlets;

import bean.CartBean;

import java.io.IOException;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import entities.Order;
public class CartController extends HttpServlet {
  
 //public static final String addToCart
  
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Order order= new Order();

public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
 
  String strAction = request.getParameter("action");
  ArrayList<String> products = new ArrayList<>();
	 ArrayList<Integer> quantities = new ArrayList<>();
	 ArrayList<Float> unitPrices = new ArrayList<>();
	 ArrayList<Float> totalPrices = new ArrayList<>();
   
  if(strAction!=null && !strAction.equals("")) {
   if(strAction.equals("add")) {
    addToCart(request);
   } else if (strAction.equals("Update")) {
    updateCart(request);
   } else if (strAction.equals("Delete")) {
    deleteCart(request);
   }
   else if (strAction.contentEquals("Delete All")){
	   deleteAll(request);
   }
  }
  response.sendRedirect("../ShoppingCart.jsp");
 }
  
 protected void deleteCart(HttpServletRequest request) {
  HttpSession session = request.getSession();
  String strItemIndex = request.getParameter("itemIndex");
  CartBean cartBean = null;
   
  Object objCartBean = session.getAttribute("cart");
  if(objCartBean!=null) {
   cartBean = (CartBean) objCartBean ;
  } else {
   cartBean = new CartBean();
  }
  order.getProducts().remove(Integer.parseInt(strItemIndex)-1);
  order.getQuantities().remove(Integer.parseInt(strItemIndex)-1);
  order.getUnitPrices().remove(Integer.parseInt(strItemIndex)-1);
  cartBean.deleteCartItem(strItemIndex);
  
 }
 
 
 protected void deleteAll(HttpServletRequest request) {
	  HttpSession session = request.getSession();
	
	  CartBean cartBean = null;
	
	   cartBean = new CartBean();
	  
	  order.getProducts().clear();
	  order.getQuantities().clear();
	  order.getUnitPrices().clear();
	
	  
	 }
  
 protected void updateCart(HttpServletRequest request) {
  HttpSession session = request.getSession();
  String strQuantity = request.getParameter("quantity");
  String strItemIndex = request.getParameter("itemIndex");
  
  CartBean cartBean = null;
   
  Object objCartBean = session.getAttribute("cart");
  if(objCartBean!=null) {
   cartBean = (CartBean) objCartBean ;
  } else {
   cartBean = new CartBean();
  }
  order.getQuantities().set(Integer.parseInt(strItemIndex) - 1, Integer.parseInt(strQuantity));


  cartBean.updateCartItem(strItemIndex, strQuantity);
 }
  
 protected void addToCart(HttpServletRequest request) {
  HttpSession session = request.getSession();
  String strModelNo = request.getParameter("modelNo");
  String strDescription = request.getParameter("description");
  String strPrice = request.getParameter("price");
  String strQuantity = request.getParameter("quantity");
   
  CartBean cartBean = null;
   
  Object objCartBean = session.getAttribute("cart");
 
  if(objCartBean!=null) {
   cartBean = (CartBean) objCartBean ;
  } else {
   cartBean = new CartBean();
   session.setAttribute("cart", cartBean);
   order = new Order();
   
   
  }
   System.out.println(" in controller" + strModelNo + "  " + strPrice +" altcv " + strQuantity +" cv " +strDescription);
   order.getProducts().add(strDescription);
   order.getQuantities().add(Integer.parseInt(strQuantity));
   order.getUnitPrices().add(Float.parseFloat(strPrice));
   session.setAttribute("orderdet", order);
  cartBean.addCartItem(strModelNo, strDescription, strPrice, strQuantity);
  
 }
 
}




