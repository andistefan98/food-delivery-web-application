package modal;

import java.io.IOException;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import bean.LogInBean;
import dao.LoginDao;
 
public class LoginServlet extends HttpServlet {
private static final long serialVersionUID = 1L;
 
public LoginServlet() {
}
 
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
{
	
 String userName = request.getParameter("userName");
 String password = request.getParameter("password");
 
 LogInBean loginBean = new LogInBean();
 
 loginBean.setUserName(userName);
 loginBean.setPassword(password);
 
 LoginDao loginDao = new LoginDao();

 try
 {
 String userValidate = loginDao.authenticateUser(loginBean);
 
 if(userValidate.equals("Admin_Role"))
 {
 System.out.println("Admin's Home");
 
 HttpSession session = request.getSession(); //Creating a session
 session.setAttribute("adminsession", loginBean); //setting session attribute
 request.setAttribute("userName", userName);
 
 request.getRequestDispatcher("/indexAdmin.jsp").forward(request, response);
 }
 
 else if(userValidate.equals("User_Role"))
 {
 System.out.println("User's Home");
 
 HttpSession session = request.getSession();
 session.setMaxInactiveInterval(10*60);
 session.setAttribute("usersession", loginBean);
 request.setAttribute("userName", userName);
 
 request.getServletContext().getRequestDispatcher("/indexClient.jsp").forward(request, response);
 }
 
 else if(userValidate.equals("Dispatcher_Role"))
 {
 System.out.println("Dispatcher's Home");
 
 HttpSession session = request.getSession();
 session.setMaxInactiveInterval(10*60);
 session.setAttribute("dispatchersession", loginBean);
 request.setAttribute("userName", userName);
 
 request.getServletContext().getRequestDispatcher("/indexDispatcher.jsp").forward(request, response);
 }
 
 else
 {
 System.out.println("Error message = "+userValidate);
 request.setAttribute("errMessage", userValidate);
 
 request.getRequestDispatcher("/LogIn.jsp").forward(request, response);
 }
 }
 catch (IOException e1)
 {
 e1.printStackTrace();
 }
 catch (Exception e2)
 {
 e2.printStackTrace();
 }
} //End of doPost()
}