package modal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bean.LogInBean;
import bean.SignUpBean;
import common_things.DB_Connection;

public class SignUp_modal {
	
	
	 public boolean register_user(SignUpBean obj_SignUp) throws SQLException{
		 boolean flag= false;
		 
		 DB_Connection newCon = new DB_Connection();
		 Connection conn = newCon.getConnection();
		 
		 PreparedStatement ps = null;
		 ResultSet rs = null;
		 
		 try{
			 String query ="select * from admin where username=? and password=?"    ;
			 ps = conn.prepareStatement(query);
			 //ps.setString(1, obj_Login.getUserName());
			 //ps.setString(2, obj_Login.getPassword());
			 rs=ps.executeQuery();
			 
			 if(rs.next())
			 {
				 flag=true;
			 }
			 
			 
		 }
		 catch(Exception e){
			e.getMessage(); 
		 }
		 finally{
			 if (conn != null)
			 {
				 conn.close();
			 }
		 }
		 
		 return flag;
	 }

}
