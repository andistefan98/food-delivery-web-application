package modal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bean.LogInBean;
import bean.SignUpBean;
import common_things.DB_Connection;

public class LogIn_modal {

	 public boolean check_user(LogInBean obj_Login) throws SQLException{
		 boolean flag= false;
		 
		 DB_Connection newCon = new DB_Connection();
		 Connection conn = newCon.getConnection();
		 
		 PreparedStatement ps = null;
		 ResultSet rs = null;
		 
		 PreparedStatement ps2 = null;
		 ResultSet rs2 = null;
		 
		 try{
			 String query ="select * from admin where username=? and password=?";
			 ps = conn.prepareStatement(query);
			 ps.setString(1, obj_Login.getUserName());
			 ps.setString(2, obj_Login.getPassword());
			 rs=ps.executeQuery();
			 
			 String query2 ="select * from user where username=? and password=?";
			 ps2 = conn.prepareStatement(query2);
			 ps2.setString(1, obj_Login.getUserName());
			 ps2.setString(2, obj_Login.getPassword());
			 rs2=ps2.executeQuery();
			 
			 if(rs2.next() || rs.next())
			 {
				 flag=true;
			 }
			 
			 
		 }
		 catch(Exception e){
			e.getMessage(); 
		 }
		 finally{
			 if (conn != null)
			 {
				 conn.close();
			 }
		 }
		 
		 return flag;
	 }
	 
}
