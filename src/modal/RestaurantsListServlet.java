package modal;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common_things.DB_Connection;

/**
 * Servlet implementation class RestaurantsListServlet
 */
@WebServlet(urlPatterns ={ "/pageRestaurantAiew"})
public class RestaurantsListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RestaurantsListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection con = null;
		 Statement statement = null;
		 ResultSet resultSet = null;
		 
		 ArrayList<String> restName = new ArrayList<String>(); 
		 ArrayList<String> restLocation= new ArrayList<String>();
		 ArrayList<String> restDescr= new ArrayList<String>();
		 ArrayList<String> restPhotos = new ArrayList<String>();
		 
		 try
		 {
		 con = DB_Connection.getConnection();
		 statement = con.createStatement();

		 resultSet = statement.executeQuery("select Name,Location,Description,Picture from restaurant");
		 
		 while(resultSet.next())
		 {
		 restName.add(resultSet.getString("Name"));
		 restLocation.add(resultSet.getString("Location"));
		 restDescr.add(resultSet.getString("Description"));
		 restPhotos.add(resultSet.getString("Picture"));
		 
		 System.out.println(restName.get(0) + " " + restLocation.get(0) + " " + restPhotos.get(0) );
		 System.out.println(restName.get(1) + " " + restLocation.get(1) + " " + restPhotos.get(1) );
		
		 }
		 
		 request.setAttribute("name",restName); // 
		 request.setAttribute("location",restLocation); // 
		 request.setAttribute("descr",restDescr); // 
		 request.setAttribute("photo",restPhotos.get(0)); // 
	     request.getRequestDispatcher("/pageRestaurantAView.jsp").forward(request, response);
		 
		 
		 }
		 catch(Exception e){}
		 
		 RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/pageRestaurantAView.jsp");
		 dispatcher.forward(request, response);
	}

	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		Connection con = null;
		 Statement statement = null;
		 ResultSet resultSet = null;
		 
		 ArrayList<String> restName = new ArrayList<String>(); 
		 ArrayList<String> restLocation= new ArrayList<String>();
		 ArrayList<String> restDescr= new ArrayList<String>();
		 ArrayList<String> restPhotos = new ArrayList<String>();
		 
		 try
		 {
		 con = DB_Connection.getConnection();
		 statement = con.createStatement();
		 System.out.println("AICI MUIE");
		 resultSet = statement.executeQuery("select Name,Location,Description,Picture from restaurant");
		 
		 while(resultSet.next())
		 {
		 restName.add(resultSet.getString("Name"));
		 restLocation.add(resultSet.getString("Location"));
		 restDescr.add(resultSet.getString("Description"));
		 restPhotos.add(resultSet.getString("Picture"));
		 
		 System.out.println(restName.get(0) + " " + restLocation.get(0) + " " + restPhotos.get(0) );
		 System.out.println(restName.get(1) + " " + restLocation.get(1) + " " + restPhotos.get(1) );
		      }  
		 }
		 catch(Exception e){}
		 
	}
	
}
