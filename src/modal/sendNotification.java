package modal;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.Order;

/**
 * Servlet implementation class sendNotification
 */
@WebServlet("/sendNotification")
public class sendNotification  extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public sendNotification() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	service(request,response);
	}
	
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException
	{	
		String ord = (String) request.getParameter("finalPrice");
	    
		System.out.println("priceeee" + ord);
	//request.setAttribute("price", ord);
		request.setAttribute("Price", ord);
	((HttpServletResponse) response).sendRedirect("http://localhost:8010/FoodDelivery/paymentOptions.jsp");
	
	}

}
