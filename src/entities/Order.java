package entities;

import java.util.ArrayList;

public class Order {

	 ArrayList<String> products = new ArrayList<>();
	 ArrayList<Integer> quantities = new ArrayList<>();
	 ArrayList<Float> unitPrices = new ArrayList<>();
		//ArrayList<Float> Totalprices = new ArrayList<>();

	 float finalPrice;

	 
	 public ArrayList<Integer> getQuantities() {
		return quantities;
	}
	public void setQuantities(ArrayList<Integer> quantities) {
		this.quantities = quantities;
	}
	public ArrayList<Float> getUnitPrices() {
		return unitPrices;
	}
	public void setUnitPrices(ArrayList<Float> unitPrices) {
		this.unitPrices = unitPrices;
	}

	public float getFinalPrice() {
		return finalPrice;
	}
	public void setFinalPrice(float finalPrice) {
		this.finalPrice = finalPrice;
	}

	 
	 public ArrayList<String> getProducts() {
		return products;
	}
	public void setProducts(ArrayList<String> items) {
		this.products = items;
	}

	 
}
